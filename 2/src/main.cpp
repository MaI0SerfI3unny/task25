#include <iostream>
#include "cpu.h"
#include "ram.h"
#include "disk.h"
#include "gpu.h"
#include "kbd.h"
using namespace std;

int main()
{
    string command;
    while (true)
    {
        cout << "> ";
        cin >> command;
        if (command == "sum") compute();
        else if (command == "save") save();
        else if (command == "load") load();
        else if (command == "input") input();
        else if (command == "display") display();
        else if (command == "exit") break;
        else std::cout << "Invalid command\n";
    }
    return 0;
}