#include "ram.h"
#include "cpu.h"
#include <iostream>

void compute()
{
    int sum = 0;
    for (int i = 0; i < 8; i++)
    {
        sum += read(i);
    }
    std::cout << "Result: " << sum << '\n';
}