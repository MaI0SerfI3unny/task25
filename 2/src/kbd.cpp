#include "kbd.h"
#include "ram.h"
#include <iostream>

void input()
{
    for (int i = 0; i < 8; i++)
    {
        int value;
        std::cin >> value;
        write(i, value);
    }
}