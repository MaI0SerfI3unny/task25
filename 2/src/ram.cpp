#include "ram.h"

int buffer[8] = {0};

void write(int index, int value)
{
    buffer[index] = value;
}

int read(int index)
{
    return buffer[index];
}
