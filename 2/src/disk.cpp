#include "disk.h"
#include "ram.h"
#include <fstream>

void save()
{
    std::ofstream file("data.txt");
    for (int i = 0; i < 8; i++)
    {
        file << read(i) << ' ';
    }
    file.close();
}

void load()
{
    std::ifstream file("data.txt");
    int value;
    int index = 0;
    while (file >> value)
    {
        write(index++, value);
    }
    file.close();
}