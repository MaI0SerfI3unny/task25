cmake_minimum_required(VERSION 3.24)
project(Computer)

set(CMAKE_CXX_STANDARD 17)
add_subdirectory(src)