#pragma once

struct Point {
    double x, y;
};

void Scalpel(Point start, Point end) {
    std::cout << "Cut made between points (" << start.x << ", " << start.y << ") and (" << end.x << ", " << end.y << ")" << std::endl;
}

void Hemostat(Point point) {
    std::cout << "Clamp applied at point (" << point.x << ", " << point.y << ")" << std::endl;
}

void Tweezers(Point point) {
    std::cout << "Tweezers applied at point (" << point.x << ", " << point.y << ")" << std::endl;
}

void Suture(Point start, Point end) {
    std::cout << "Suture applied between points (" << start.x << ", " << start.y << ") and (" << end.x << ", " << end.y << ")" << std::endl;
}