cmake_minimum_required(VERSION 3.24)
project(Medicine)

set(CMAKE_CXX_STANDARD 17)

add_executable(Medicine main.cpp include/medic_instruments.h)
target_include_directories(Medicine PUBLIC include)