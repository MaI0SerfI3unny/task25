#include <iostream>
#include "include/medic_instruments.h"
using namespace std;

int main() {
    Point start, end;
    cout << "Enter start point:"<<endl;
    cin >> start.x >> start.y;
    cout << "Enter end point:"<<endl;
    cin >> end.x >> end.y;

    Scalpel(start, end);
    Hemostat(start);
    Tweezers(end);
    Suture(start, end);

    if (start.x == end.x && start.y == end.y) {
        cout << "Operation completed successfully" << endl;
    }

    return 0;
}
